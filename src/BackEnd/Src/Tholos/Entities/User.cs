/*
* Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Tholos.Entities
{
    /// <summary>
    /// This represents user.
    /// </summary>
    /// 
    /// <remarks>
    /// <para>
    /// This class is marked with the <see cref="DataContractAttribute"/> attribute
    /// and the properties with the <see cref="DataMemberAttribute"/> attribute.
    /// </para>
    /// </remarks>
    ///
    /// <threadsafety>
    /// This class is mutable, so it is not thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [DataContract]
    public class User : NamedEntity
    {
        /// <summary>
        /// Gets or sets the Name.
        /// </summary>
        /// <remarks>
        /// This override the property of base class in order to specify validation rules.
        /// </remarks>
        /// <value>The user Name</value>
        [DataMember]
        [Required]
        [StringLength(500)]
        public override string Name { get; set; }

        /// <summary>
        /// Gets or sets the Username.
        /// </summary>
        /// <value>The Username</value>
        [DataMember]
        [Required]
        [StringLength(64)]
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets roles.
        /// </summary>
        /// <value>The  roles.</value>
        [DataMember]
        public IList<Role> Roles
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets userpic path.
        /// </summary>
        /// <value>The  userpic path.</value>
        [DataMember]
        [Required]
        [MaxLength(1024)]
        public string UserpicPath
        {
            get;
            set;
        }

        /// <summary>
        /// Initializes new instance of the <see cref="User"/> class.
        /// </summary>
        public User()
        {
        }
    }
}

