/*
* Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Tholos.Entities
{
    /// <summary>
    /// This represents prototype test.
    /// </summary>
    /// 
    /// <remarks>
    /// <para>
    /// This class is marked with the <see cref="DataContractAttribute"/> attribute
    /// and the properties with the <see cref="DataMemberAttribute"/> attribute.
    /// </para>
    /// </remarks>
    ///
    /// <threadsafety>
    /// This class is mutable, so it is not thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [DataContract]
    public class PrototypeTest : IdentifiableEntity
    {
        /// <summary>
        /// Gets or sets prototype code.
        /// </summary>
        /// <value>The prototype code.</value>
        [DataMember]
        [Required]
        [MaxLength(64)]
        public string PrototypeCode
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets iteration.
        /// </summary>
        /// <value>The iteration.</value>
        [DataMember]
        [Required]
        public int Iteration
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets answers.
        /// </summary>
        /// <value>The answers.</value>
        [DataMember]
        public IList<Answer> Answers
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets before weight.
        /// </summary>
        /// <value>The before weight.</value>
        [DataMember]
        [Required]
        public double BeforeWeight
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets before photo local path.
        /// </summary>
        /// <value>The before photo local path.</value>
        [DataMember]
        [MaxLength(1024)]
        public string BeforePhotoLocalPath
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets before photo uri.
        /// </summary>
        /// <value>The before photo uri.</value>
        [DataMember]
        [Required]
        [MaxLength(1024)]
        public string BeforePhotoUri
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets after weight.
        /// </summary>
        /// <value>The after weight.</value>
        [DataMember]
        public double? AfterWeight
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets after photo local path.
        /// </summary>
        /// <value>The after photo local path.</value>
        [DataMember]
        [MaxLength(1024)]
        public string AfterPhotoLocalPath
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets after photo uri.
        /// </summary>
        /// <value>The after photo uri.</value>
        [DataMember]
        [MaxLength(1024)]
        public string AfterPhotoUri
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets completed id.
        /// </summary>
        /// <value>
        /// <c>true</c> if completed; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        [Required]
        public bool Completed
        {
            get;
            set;
        }

        /// <summary>
        /// <para>
        /// Initializes new instance of the <see cref="PrototypeTest"/> class.
        /// </para>
        /// </summary>
        public PrototypeTest()
        {
        }
    }
}

