/*
* Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Tholos.Entities
{
    /// <summary>
    /// An entity class that represents NamedEntity.
    /// </summary>
    /// 
    /// <remarks>
    /// <para>
    /// This class is marked with the <see cref="DataContractAttribute"/> attribute
    /// and the properties with the <see cref="DataMemberAttribute"/> attribute.
    /// </para>
    /// </remarks>
    ///
    /// <threadsafety>
    /// This class is mutable, so it is not thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [DataContract]
    public abstract class NamedEntity : IdentifiableEntity
    {

        /// <summary>
        /// Gets or sets the entity name.
        /// </summary>
        /// <value>
        /// The entity name.
        /// </value>
        [DataMember]
        [Required]
        public virtual string Name { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="NamedEntity"/> class.
        /// </summary>
        protected NamedEntity()
        {
        }
    }
}

