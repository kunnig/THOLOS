﻿/*
* Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.Runtime.Serialization;

namespace Tholos.Entities
{
    /// <summary>
    /// This represents IndexedQuestion.
    /// </summary>
    /// 
    /// <remarks>
    /// <para>
    /// This class is marked with the <see cref="DataContractAttribute"/> attribute
    /// and the properties with the <see cref="DataMemberAttribute"/> attribute.
    /// </para>
    /// </remarks>
    ///
    /// <threadsafety>
    /// This class is mutable, so it is not thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [DataContract]
    public class IndexedQuestion
    {
        /// <summary>
        /// Gets or sets position.
        /// </summary>
        /// <value>The position.</value>
        [DataMember]
        public int Position { get; set; }

        /// <summary>
        /// Gets or sets question.
        /// </summary>
        /// <value>The question.</value>
        [DataMember]
        public Question Question { get; set; }
    }
}
