/*
*  Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.ServiceModel.Web;

namespace Tholos.Services.Impl
{

    /// <summary>
    /// This class is the realization of the <see cref="IAuthenticationService"/> service contract. 
    /// It handles the authentication check.
    /// </summary>
    ///
    /// <threadsafety>
    /// This class is mutable (base class is mutable) but effectively thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    public class AuthenticationService : BasePersistenceService, IAuthenticationService
    {
        /// <summary>
        /// Represents the name of the stored procedure to check username and password of participant.
        /// </summary>
        private const string CheckParticipantUsernamePasswordSpName = "spCheckParticipantUsernamePassword";

        /// <summary>
        /// Represents the name of the stored procedure to check username and password of user.
        /// </summary>
        private const string CheckUserUsernamePasswordSpName = "spCheckUserUsernamePassword";

        /// <summary>
        /// <para>
        /// Initializes a new instance of the <see cref="AuthenticationService"/> class.
        /// </para>
        /// </summary>
        public AuthenticationService()
        {
        }

        /// <summary>
        /// <para>
        /// Performs authentication.
        /// </para>
        /// </summary>
        /// <remarks>It will check for both - Manager and Participant authentication.
        ///  Manager password will be hashed before checking</remarks>
        /// 
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// 
        /// <returns> <c>true</c> if authenticated, <c>false</c> otherwise. </returns>
        /// 
        /// <exception cref="WebFaultException">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/>If <paramref name="username"/> or <paramref name="password"/> is 
        ///     <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/>If username or password is empty.</item>
        /// <item><see cref="PersistenceException"/>If a DB-based error occurs. </item>
        /// <item><see cref="TholosException"/>If any other errors occur while performing this operation.</item>
        /// </list></para></exception>
        public bool Authenticate(string username, string password)
        {
            return Helper.LoggingWrapper(Logger, () =>
            {
                Helper.CheckNotNullOrEmpty(username, "username");
                Helper.CheckNotNullOrEmpty(password, "password");
                var hashedPassword = SecurityHelper.HashSHA1(password);
                using (var conn = GetConnection())
                {
                    // check for participant
                    var records =
                        Convert.ToInt16(Helper.ExecuteRetrieveSp(conn, CheckParticipantUsernamePasswordSpName,
                            (reader) =>
                            {
                                if (reader.Read())
                                {
                                    return reader["IsExist"];
                                }
                                return false;
                            }
                            , new Dictionary<string, object>
                            {
                                {"Username", username},
                                {"Password", SecurityHelper.Encrypt(password)}
                            }));

                    if (records > 0)
                    {
                        return true;
                    }

                    // check for manager
                    records =
                        Convert.ToInt16(Helper.ExecuteRetrieveSp(conn, CheckUserUsernamePasswordSpName,
                            (reader) =>
                            {
                                if (reader.Read())
                                {
                                    return reader["IsExist"];
                                }
                                return false;
                            }, new Dictionary<string, object>
                            {
                                {"Username", username},
                                {"Password", hashedPassword}
                            }));

                    if (records > 0)
                    {
                        return true;
                    }

                    return false;
                }
            }, username, Helper.Mask);
        }
    }
}

