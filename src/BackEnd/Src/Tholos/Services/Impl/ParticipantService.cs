/*
*  Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.ServiceModel;
using System.ServiceModel.Web;
using Microsoft.Practices.Unity;
using Tholos.Entities;
using IsolationLevel = System.Transactions.IsolationLevel;

namespace Tholos.Services.Impl
{

    /// <summary>
    /// <para>
    /// This class is the realization of the <see cref="IParticipantService"/> service contract.
    /// It handles all the participant related operations.
    /// </para>
    /// <para>
    /// Changes in version 1.1 (THOLOS - Integrate Prototype with Backend APIs):
    /// <list type="bullet">
    /// <item>
    /// <description>Changed CreateParticipant to use participant's username as key for search.</description>
    /// </item>
    /// </list>
    /// </para>
    /// </summary>
    ///
    /// <threadsafety>
    /// This class is mutable (base class is mutable) but effectively thread-safe.
    /// </threadsafety>
    /// 
    /// <author>veshu</author>
    /// <author>duxiaoyang</author>
    ///
    /// <version>1.1</version>
    /// <since>1.0</since>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple,
        InstanceContextMode = InstanceContextMode.PerSession,
        ReleaseServiceInstanceOnTransactionComplete = false,
        TransactionIsolationLevel = IsolationLevel.RepeatableRead)]
    public class ParticipantService : BasePersistenceService, IParticipantService
    {
        #region used stored procedures names
        /// <summary>
        /// Represents the name of the stored procedure to retrieve participant credentials by username.
        /// </summary>
        private const string GetParticipantCredentialsByUsernameSpName = "spGetParticipantCredentialsByUsername";

        /// <summary>
        /// Represents the name of the stored procedure to create participant.
        /// </summary>
        private const string CreateParticipantSpName = "spCreateParticipant";

        /// <summary>
        /// Represents the name of the stored procedure to get role id by name.
        /// </summary>
        private const string GetRoleIdByNameSpName = "spGetRoleIdByName";

        /// <summary>
        /// Represents the name of the stored procedure to create participant role.
        /// </summary>
        private const string CreateParticipantRoleSpName = "spCreateParticipantRole";

        /// <summary>
        /// Represents the name of the stored procedure to create profile answer.
        /// </summary>
        private const string CreateProfileAnswerSpName = "spCreateProfileAnswer";

        /// <summary>
        /// Represents the name of the stored procedure to update the participant credentials as used by id.
        /// </summary>
        private const string UpdateParticipantCredentialsAsUsedByIdSpName =
            "spUpdateParticipantCredentialsAsUsedById";

        /// <summary>
        /// Represents the name of the stored procedure to get participant.
        /// </summary>
        private const string GetParticipantSpName = "spGetParticipant";

        /// <summary>
        /// Represents the name of the stored procedure to get profile answer by participant id.
        /// </summary>
        private const string GetProfileAnswersByParticipantIdSpName = "spGetProfileAnswersByParticipantId";

        /// <summary>
        /// Represents the name of the stored procedure to get participant by username.
        /// </summary>
        private const string GetParticipantIdByUsernameSpName = "spGetParticipantIdByUsername";

        /// <summary>
        /// Represents the name of the stored procedure to get all profile questions.
        /// </summary>
        private const string GetAllProFileQuestionsSpName = "spGetAllProFileQuestions";

        /// <summary>
        /// Represents the name of the stored procedure to get profile answer options by question id.
        /// </summary>
        private const string GetProfileAnswerOptionsByQuestionIdSpName = "spGetProfileAnswerOptionsByQuestionId";
        #endregion

        /// <summary>
        /// <para>
        /// Represents the participant role name.
        /// </para>
        /// </summary>
        /// <remarks>The participant role name. It should not be null or empty . It is optional,
        /// defaults to "Participant".</remarks>
        private string _participantRole = "Participant";

        /// <summary>
        /// <para>
        /// Gets or sets the participant role name.
        /// </para>
        /// </summary>
        /// <remarks>The participant role name. It should not be null or empty . It is optional,
        /// defaults to "Participant".</remarks>
        [Dependency("ParticipantRole")]
        public string ParticipantRole
        {
            get
            {
                return _participantRole;
            }
            set
            {
                _participantRole = value;
            }
        }

        /// <summary>
        /// <para>
        /// Initializes a new instance of the <see cref="ParticipantService"/> class.
        /// </para>
        /// </summary>
        public ParticipantService()
        {

        }

        /// <summary>
        /// Creates the given participant, and returns the ID of the created entity.
        /// </summary>
        /// <remarks><para>It will mark the corresondent credentials as used 
        /// they can't be used by another user to create new participant.</para></remarks>
        /// 
        /// <param name="participant">The participant to create.</param>
        ///
        /// <returns>The Id of the created participant.</returns>
        ///
        /// <exception cref="WebFaultException{T}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> If <paramref name="participant"/> is null.</item>
        /// <item><see cref="EntityNotFoundException"/> If entity does not exist.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list> </para> </exception>
        [OperationBehavior(TransactionScopeRequired = true, TransactionAutoComplete = true)]
        public long CreateParticipant(Participant participant)
        {
            return Helper.LoggingWrapper(Logger, () =>
            {
                Helper.ValidateParticipant(participant);

                using (var conn = GetConnection())
                {
                    var participantCredentials = Helper.ExecuteRetrieveSp(conn,
                        GetParticipantCredentialsByUsernameSpName, Helper.ReaderParticipantCredentials,
                        new Dictionary<string, object>
                        {
                            {"Username", participant.Username}
                        });
                    if (participantCredentials == null)
                    {
                        throw new EntityNotFoundException(
                            string.Format("No participant credentail is found for username {0}"
                                , participant.Username));
                    }
                    participant.SurveyId = participantCredentials.SurveyId;

                    var participantId = Helper.ExecuteSpWithReturnOutParam(conn, CreateParticipantSpName,
                        new Dictionary<string, object>
                        {
                            {"Username", participant.Username},
                            {"SurveyId", participantCredentials.SurveyId},
                            {"BirthDate", participant.BirthDate},
                            {"Weight", participant.Weight},
                            {"HeightFeet", participant.HeightFeet},
                            {"HeightInches", participant.HeightInches}
                       }, "Id");
                    if (participantId <= 0)
                    {
                        throw new EntityNotFoundException(
                            string.Format(
                                "No participant credential is available for Username '{0}', SurveyId '{1}'.",
                                participant.Username, participant.SurveyId));
                    }

                    var roles = Helper.ExecuteRetrieveSp(conn, GetRoleIdByNameSpName, Helper.ReaderRoles,
                        new Dictionary<string, object>
                        {
                            {"Name", ParticipantRole}
                        });

                    if (roles != null)
                    {
                        if (roles.Count < 1)
                        {
                            throw new TholosException(
                                string.Format("No roles are found with role name {0}", ParticipantRole));
                        }

                        if (roles.Count > 1)
                        {
                            throw new TholosException(
                                string.Format("Multiple roles are found with role name {0}", ParticipantRole));
                        }

                        Helper.ExecuteSp(conn, CreateParticipantRoleSpName, new Dictionary<string, object>
                        {
                            {"ParticipantId", participantId},
                            {"RoleId", roles[0].Id}
                        });

                        if (participant.ProfileAnswers != null)
                        {
                            foreach (var profileAnswer in participant.ProfileAnswers)
                            {
                                var paramaters = Helper.BuildAnswerParameter(profileAnswer);
                                paramaters.Add("ParticipantId", participantId);
                                Helper.ExecuteSpWithReturnOutParam(conn, CreateProfileAnswerSpName,
                                    paramaters, "Id");
                            }
                        }
                        Helper.ExecuteUpdateOrDeleteSp<ParticipantCredentials>(conn,
                            UpdateParticipantCredentialsAsUsedByIdSpName, "Id=" + participant.Id
                            , new Dictionary<string, object>
                            {
                                {"Id", participantCredentials.Id}
                            });
                    }
                    else
                    {
                        throw new EntityNotFoundException(string.Format("No Role is found for Name '{0}'.",
                            ParticipantRole));
                    }
                    return participantId;
                }
            }, participant);
        }

        /// <summary>
        /// Gets the participant by the given Id.
        /// </summary>
        ///
        /// <param name="participantId">The Id of the participant to retrieve.</param>
        ///
        /// <returns>The participant corresponding to the given Id,
        /// or <c>null</c> if none found.</returns>
        ///
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/>If <paramref name="participantId"/> is <c>null</c>.</item>
        /// <item>
        /// <see cref="ArgumentException"/> If <paramref name="participantId"/> is not valid positive number.
        /// </item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur
        /// while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationBehavior(TransactionScopeRequired = false)]
        public Participant GetParticipantById(string participantId)
        {
            return Helper.LoggingWrapper(Logger, () =>
            {
                Helper.CheckNotNullOrEmpty(participantId, "participantId");
                var participantIdValue = Helper.CheckAndGetValidNumberAndPositive(participantId, "participantId");
                using (var conn = GetConnection())
                {
                    return GetParticipant(conn, participantIdValue);
                }
            }, participantId);
        }

        /// <summary>
        /// Gets the participant from database.
        /// </summary>
        /// <param name="conn">The opened database connection.</param>
        /// <param name="participantId">The participant id.</param>
        /// <returns>The participant.</returns>
        private Participant GetParticipant(IDbConnection conn, long participantId)
        {
            var participant = Helper.ExecuteRetrieveSp(conn, GetParticipantSpName,
                   Helper.ReaderParticipant
                   , new Dictionary<string, object>
                   {
                       {"ParticipantId", participantId}
                   });
            if (participant != null)
            {
                participant.ProfileAnswers = new List<Answer>();
                participant.ProfileAnswers = Helper.ExecuteRetrieveSp(conn
                    , GetProfileAnswersByParticipantIdSpName,
                    Helper.ReaderAnswer, new Dictionary<string, object>
                    {
                        {"ParticipantId", participantId}
                    });
            }
            return participant;
        }

        /// <summary>
        /// Gets the participant by the given username.
        /// </summary>
        ///
        /// <param name="username">The username of the participant to retrieve.</param>
        ///
        /// <returns>The participant corresponding to the given username,
        /// or <c>null</c> if none found.</returns>
        ///
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> If <paramref name="username"/> is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/> If <paramref name="username"/> is empty.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur
        /// while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationBehavior(TransactionScopeRequired = false)]
        public Participant GetParticipantByUserName(string username)
        {
            return Helper.LoggingWrapper(Logger, () =>
            {
                Helper.CheckNotNullOrEmpty(username, "username");
                using (var conn = GetConnection())
                {
                    var participantId = Convert.ToInt64(Helper.ExecuteRetrieveSp(conn
                        , GetParticipantIdByUsernameSpName,
                        (reader) =>
                        {
                            if (reader.Read())
                            {
                                return reader["Id"];
                            }
                            else
                            {
                                return 0;
                            }
                        }, new Dictionary<string, object>
                        {
                            {"username", username}
                        }));

                    return GetParticipant(conn, participantId);
                }
            }, username);
        }

        /// <summary>
        /// Gets the profile questions fully populated with the answer options.
        /// </summary>
        ///
        /// <returns>The profile questions.</returns>
        ///
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationBehavior(TransactionScopeRequired = false)]
        public IList<Question> GetAllProfileQuestions()
        {
            return Helper.LoggingWrapper(Logger, () =>
            {
                using (var conn = GetConnection())
                {
                    var questions = Helper.ExecuteRetrieveSp(conn, GetAllProFileQuestionsSpName,
                        Helper.ReaderQuestionsList);

                    foreach (var question in questions)
                    {
                        question.AnswerOptions =
                            Helper.ExecuteRetrieveSp(conn, GetProfileAnswerOptionsByQuestionIdSpName,
                                Helper.ReaderAnswerOptions, new Dictionary<string, object>
                                {
                                    {"QuestionId", question.Id}
                                });
                    }
                    return questions;
                }
            });
        }

        /// <summary>
        /// <para>
        /// Checks whether this instance was properly configured.
        /// </para>
        /// </summary>
        ///
        /// <exception cref="ConfigurationException ">
        /// If it is not configured.
        /// </exception>
        public override void CheckConfiguration()
        {
            base.CheckConfiguration();
            Helper.CheckConfiguration(ParticipantRole, "ParticipantRole");
        }
    }
}

