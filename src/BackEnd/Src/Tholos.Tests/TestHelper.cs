/*
 * Copyright (c) 2015, TopCoder, Inc. All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.ServiceModel.Web;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Tholos.Entities;
using Tholos.Services;
using Tholos.Services.Impl;
using ConfigurationException = Tholos.Services.ConfigurationException;

namespace Tholos.Tests
{
    /// <summary>
    /// <para>
    /// This class provides common methods used in unit tests.
    /// </para>
    /// <para>
    /// Changes in version 1.1 (THOLOS - Integrate Prototype with Backend APIs):
    /// <list type="bullet">
    /// <item>
    /// <description>Added the option to send request using participant account.</description>
    /// </item>
    /// </list>
    /// </para>
    /// </summary>
    /// 
    /// <author>veshu</author>
    /// <author>duxiaoyang</author>
    ///
    /// <version>1.1</version>
    /// <since>1.0</since>
    ///
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    internal static class TestHelper
    {
        /// <summary>
        /// Represents empty string used for failure tests.
        /// </summary>
        internal const string EmptyString = " \r \t \n  ";

        /// <summary>
        /// Represents key name of connection string name.
        /// </summary>
        internal const string ConnectionStringName = "Default";

        /// <summary>
        /// Represents key name of database type.
        /// </summary>
        internal const string DatabaseTypeName = "DatabaseTypeName";

        /// <summary>
        /// The WCF service host base URI.
        /// </summary>
        internal static readonly string WcfServiceHostBaseUri =
            ConfigurationManager.AppSettings["WcfServiceHostBaseUri"];

        /// <summary>
        /// Represents the participant username length.
        /// </summary>
        internal const int ParticipantUsernameLength = 10;

        /// <summary>
        /// Represents the participant Password length.
        /// </summary>
        internal const int ParticipantPasswordLength = 12;

        /// <summary>
        /// Represents the file upload folder path.
        /// </summary>
        internal const string FileUploadPath = @"/uploadFile/";

        /// <summary>
        /// Represents the photo file name template.
        /// </summary>
        internal const string ProductPhotoFilenameTemplate = "{survey_id}{participant_id}{prototype_code}{iteration}";

        /// <summary>
        /// Represents the base file uri.
        /// </summary>
        internal const string BaseFileUri = @"/uploadFile/";

        /// <summary>
        /// Asserts that RESTful WCF service throws specified exception.
        /// </summary>
        /// <typeparam name="TException">The type of the exception.</typeparam>
        /// <param name="action">The action.</param>
        internal static void AssertThrows<TException>(Action action)
        {
            try
            {
                action();
                Assert.Fail("Should have thrown WebFaultException<ServiceFaultDetail> exception.");
            }
            catch (WebFaultException<ServiceFaultDetail> ex)
            {
                // assert exception type
                Assert.AreEqual(typeof(TException).Name, ex.Detail.ErrorType, "Response error type is incorrect.");

                // assert status code
                HttpStatusCode expectedStatusCode;
                Type type = typeof(TException);
                if (typeof(ArgumentException).IsAssignableFrom(type))
                {
                    expectedStatusCode = HttpStatusCode.BadRequest;
                }
                else if (type == typeof(EntityNotFoundException))
                {
                    expectedStatusCode = HttpStatusCode.NotFound;
                }
                else if (type == typeof(UnauthorizedAccessException))
                {
                    expectedStatusCode = HttpStatusCode.Unauthorized;
                }
                else
                {
                    expectedStatusCode = HttpStatusCode.InternalServerError;
                }

                Assert.AreEqual(expectedStatusCode, ex.StatusCode, "Response StatusCode is incorrect.");
            }
        }

        /// <summary>
        /// Sends the request to the RESTful WCF service and returns response string.
        /// </summary>
        /// <param name="uri">The request Uri.</param>
        /// <param name="method">The HTTP request method.</param>
        /// <param name="parameter">The optional parameter that will be sent in request body.</param>
        /// <param name="serializeWithType">if true typehandling will be all</param>
        /// <param name="asParticipant">True if the request is sent as participant</param>
        /// <returns>The response string.</returns>
        internal static string ProcessHttpRequest(string uri, string method, object parameter = null
            , bool serializeWithType = false, bool asParticipant = false)
        {
            using (var client = new HttpClient())
            {
                var byteArray = asParticipant ? Encoding.ASCII.GetBytes("participant1:CdDc4") : Encoding.ASCII.GetBytes("topcoder:password");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                    Convert.ToBase64String(byteArray));
                var content = new StringContent(
                    SerializeJSon(parameter, serializeWithType), Encoding.UTF8, "application/json");

                HttpResponseMessage response = null;
                switch (method)
                {
                    case "GET":
                        response = client.GetAsync(uri).Result;
                        break;
                    case "POST":
                        response = client.PostAsync(uri, content).Result;
                        break;
                    case "PUT":
                        response = client.PutAsync(uri, content).Result;
                        break;
                    case "DELETE":
                        response = client.DeleteAsync(uri).Result;
                        break;
                    default:
                        throw new ArgumentException("Request Method not supported.", "method");
                }

                string responseString = response.Content.ReadAsStringAsync().Result;
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    ServiceFaultDetail faultDetail =
                        JsonConvert.DeserializeObject<ServiceFaultDetail>(responseString);
                    throw new WebFaultException<ServiceFaultDetail>(faultDetail, response.StatusCode);
                }
                return responseString;
            }
        }

        /// <summary>
        /// Serailize object to json string.
        /// </summary>
        /// <param name="parameter">The object to serailize.</param>
        /// <param name="serializeWithType">if true typehandling will be all</param>
        /// <returns>The json string.</returns>
        internal static string SerializeJSon(object parameter = null, bool serializeWithType = false)
        {
            var dateFormatSettings = new JsonSerializerSettings
            {
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat,
                TypeNameHandling = TypeNameHandling.Auto
            };
            if (serializeWithType)
            {
                if (parameter.GetType().BaseType == typeof(Answer) ||
                    parameter.GetType().BaseType == typeof(AnswerOption))
                {
                    dateFormatSettings.TypeNameHandling = TypeNameHandling.All;

                }
            }
            var json = JsonConvert.SerializeObject(parameter, dateFormatSettings);
            json = json.Replace("\"$type\":\"", "\"__type\":\"");
            json = json.Replace("Tholos.Entities.SingleAnswer, Tholos", "SingleAnswer:#Tholos.Entities");
            json = json.Replace("Tholos.Entities.RangeAnswer, Tholos", "RangeAnswer:#Tholos.Entities");
            json = json.Replace("Tholos.Entities.SingleInputAnswer, Tholos", "SingleInputAnswer:#Tholos.Entities");
            json = json.Replace("Tholos.Entities.SingleAnswerOption, Tholos", "SingleAnswerOption:#Tholos.Entities");
            json = json.Replace("Tholos.Entities.RangeAnswerOption, Tholos", "RangeAnswerOption:#Tholos.Entities");
            json = json.Replace("Tholos.Entities.SingleAnswerInputOption, Tholos",
                "SingleAnswerInputOption:#Tholos.Entities");
            return json;
        }

        /// <summary>
        /// Adds test data to the database.
        /// </summary>
        internal static void FillDatabase()
        {
            DatabaseType databaseType =
                   (DatabaseType)
                       Enum.Parse(typeof(DatabaseType), ConfigurationManager.AppSettings[DatabaseTypeName], true);

            switch (databaseType)
            {
                case DatabaseType.SqlServer:
                    Execute(File.ReadAllText(@"..\..\..\..\DB\TestData.sql"));
                    break;
                case DatabaseType.MySql:
                    Execute(File.ReadAllText(@"..\..\..\..\DB\MySql\TestData.sql"));
                    break;
            }

        }

        /// <summary>
        /// Clears test data from the database.
        /// </summary>
        internal static void ClearDatabase()
        {
            DatabaseType databaseType =
                   (DatabaseType)
                       Enum.Parse(typeof(DatabaseType), ConfigurationManager.AppSettings[DatabaseTypeName], true);

            switch (databaseType)
            {
                case DatabaseType.SqlServer:
                    Execute(File.ReadAllText(@"..\..\..\..\DB\ClearTestData.sql"));
                    break;
                case DatabaseType.MySql:
                    Execute(File.ReadAllText(@"..\..\..\..\DB\MySql\ClearTestData.sql"));
                    break;
            }

        }

        /// <summary>
        /// Asserts that record exists in the database with given parameters.
        /// </summary>
        ///
        /// <param name="tableName">The table's name.</param>
        /// <param name="parameters">The SQL statement parameters.</param>
        internal static void AssertDatabaseRecordExists(string tableName, IDictionary<string, object> parameters)
        {
            AssertDatabaseRecordCount(tableName, parameters, 1);
        }

        /// <summary>
        /// Asserts that there are exactly <paramref name="expectedRecordCount"/> records exists in
        /// the database with given parameters.
        /// </summary>
        ///
        /// <param name="tableName">The table's name.</param>
        /// <param name="parameters">The SQL statement parameters.</param>
        /// <param name="expectedRecordCount">The expected matched record count.</param>
        internal static void AssertDatabaseRecordCount(string tableName,
            IDictionary<string, object> parameters, int expectedRecordCount)
        {
            StringBuilder sql = new StringBuilder();

            sql.AppendFormat("SELECT COUNT(*) FROM {0} WHERE 1 = 1", tableName);

            // Build WHERE clause
            foreach (KeyValuePair<string, object> pair in parameters)
            {
                sql.AppendFormat(" AND {0} " + (pair.Value == null ? "IS NULL" : "= @{0}"), pair.Key);
            }

            int recordCount = Convert.ToInt16(Execute(sql.ToString(), parameters));
            Assert.AreEqual(expectedRecordCount, recordCount,
                string.Format("Expected {0} records in {1} table with given parameters, but was {2}.",
                expectedRecordCount, tableName, recordCount));
        }

        /// <summary>
        /// Gets <see cref="IDbConnection"/> instance to access the persistence.
        /// </summary>
        /// <param name="type">The database type.</param>
        /// <returns>The created connection.</returns>
        private static IDbConnection GetConnection()
        {
            IDbConnection connection = null;
            try
            {
                ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[ConnectionStringName];
                if (settings == null)
                {
                    throw new ConfigurationException(
                        string.Format("Connection string with name {0} is missing.", ConnectionStringName));
                }
                DatabaseType databaseType =
                    (DatabaseType)
                        Enum.Parse(typeof(DatabaseType), ConfigurationManager.AppSettings[DatabaseTypeName], true);
                Console.WriteLine(settings.ConnectionString);
                switch (databaseType)
                {
                    case DatabaseType.SqlServer:
                        connection = new SqlConnection(settings.ConnectionString);
                        break;
                    case DatabaseType.MySql:
                        connection = new MySqlConnection(settings.ConnectionString);
                        break;
                }
                // Create and open connection
                if (connection != null)
                {
                    connection.Open();
                }
                return connection;
            }
            catch
            {
                // Be sure to dispose of connection object if it isn't null
                if (connection != null)
                {
                    connection.Dispose();
                }
                throw;
            }
        }

        /// <summary>
        /// Executes the given SQL statement.
        /// </summary>
        ///
        /// <param name="sql">The SQL statement to execute.</param>
        /// <param name="parameters">The parameters for the statement.</param>
        /// <returns>The scalar result of executed query.</returns>
        private static object Execute(string sql, IEnumerable<KeyValuePair<string, object>> parameters = null)
        {
            using (IDbConnection connection = GetConnection())
            {
                using (IDbCommand cmd = connection.CreateCommand())
                {
                    cmd.CommandText = sql;
                    if (parameters != null)
                    {
                        foreach (KeyValuePair<string, object> pair in parameters)
                        {
                            if (pair.Value == null)
                            {
                                continue;
                            }
                            IDataParameter parameter = cmd.CreateParameter();
                            parameter.ParameterName = "@" + pair.Key;
                            parameter.Value = pair.Value;
                            cmd.Parameters.Add(parameter);
                        }
                    }
                    return cmd.ExecuteScalar();
                }
            }
        }

        /// <summary>
        /// Asserts whether the input objects have equal properties or not.
        /// </summary>
        /// <typeparam name="T">The type of objects to compare</typeparam>
        /// <param name="expected">The expected.</param>
        /// <param name="actual">The actual.</param>
        internal static void AreEqual<T>(T expected, T actual)
        {
            foreach (var property in typeof(T).GetProperties())
            {
                var expectedValue = property.GetValue(expected, null);
                var actualValue = property.GetValue(actual, null);

                if (property.PropertyType.IsSimpleType())
                {

                    Assert.AreEqual(expectedValue, actualValue);
                }
                else
                {
                    AreEqual(expectedValue, actualValue);
                }
            }
        }

        /// <summary>
        /// Determine whether a type is simple (String, Decimal, DateTime, etc) 
        /// or complex (i.e. custom class with public properties and methods).
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>Whether is simple type or no.</returns>
        private static bool IsSimpleType(this Type type)
        {
            return
                type.IsValueType ||
                type.IsPrimitive ||
                new[] { 
				typeof(String),
				typeof(Decimal),
				typeof(DateTime),
				typeof(DateTimeOffset),
				typeof(TimeSpan),
				typeof(Guid)
			}.Contains(type) ||
                Convert.GetTypeCode(type) != TypeCode.Object;
        }

        /// <summary>
        /// Creates answer list to create
        /// </summary>
        /// <returns>The answers.</returns>
        internal static IList<Answer> GetAnswersList()
        {
            var answers = new List<Answer>();
            answers.Add(new SingleAnswer
            {
                AnswerType = AnswerType.SingleAnswer,
                QuestionId = 1,
                AnswerOptionId = 1
            });
            answers.Add(new RangeAnswer
            {
                AnswerType = AnswerType.RangeAnswer,
                QuestionId = 3,
                Value = 12
            });
            answers.Add(new SingleInputAnswer
            {
                AnswerType = AnswerType.SingleAnswerInput,
                QuestionId = 3,
                AnswerOptionId = 1,
                Input = "input"
            });
            return answers;
        }

        /// <summary>
        /// Verifies the accuracy of answers.
        /// </summary>
        /// <param name="answers">The answers</param>
        internal static void VerifyAnswer(IList<Answer> answers)
        {
            Assert.IsNotNull(answers, "ProfileAnswers should not be null");
            Assert.AreEqual(3, answers.Count, "ProfileAnswers is wrong");
            foreach (var answer in answers)
            {
                var type = answer.AnswerType;
                if (type == AnswerType.SingleAnswer)
                {
                    Assert.AreEqual(1, answer.Id, "SingleAnswer is wrong");
                    Assert.AreEqual(1, answer.QuestionId, "QuestionId is wrong");
                    Assert.AreEqual(1, ((SingleAnswer)answer).AnswerOptionId, "AnswerOptionId is wrong");
                }
                if (type == AnswerType.RangeAnswer)
                {
                    Assert.AreEqual(2, answer.Id, "SingleAnswer is wrong");
                    Assert.AreEqual(3, answer.QuestionId, "QuestionId is wrong");
                    Assert.AreEqual(4, ((RangeAnswer)answer).Value, "Value is wrong");
                }
                if (type == AnswerType.SingleAnswerInput)
                {
                    Assert.AreEqual(3, answer.Id, "SingleAnswer is wrong");
                    Assert.AreEqual(2, answer.QuestionId, "QuestionId is wrong");
                    Assert.AreEqual(2, ((SingleInputAnswer)answer).AnswerOptionId, "AnswerOptionId is wrong");
                    Assert.AreEqual("11", ((SingleInputAnswer)answer).Input, "Input is wrong");
                }
            }
        }

        /// <summary>
        /// Verify the accuracy of answer options of questions
        /// </summary>
        /// <param name="result">The questions</param>
        internal static void VerifyAnswerOption(IList<Question> result)
        {
            foreach (var question in result)
            {
                Assert.IsNotNull(question.AnswerOptions, "Profile answers options should not be null");
                Assert.AreNotEqual(0, question.AnswerOptions.Count, "count of anser option is wrong.");
            }
        }

        /// <summary>
        /// Deletes the temporary location used to upload file.
        /// </summary>
        internal static void ClearUploadFolder()
        {
            if (Directory.Exists(FileUploadPath))
            {
                Directory.Delete(FileUploadPath, true);
            }
        }

        /// <summary>
        /// Creates questions for creation of entity.
        /// </summary>
        /// <returns>The questions.</returns>
        internal static IList<IndexedQuestion> GetQuestionsList()
        {
            var questions = new List<IndexedQuestion>
            {
                {
                    new IndexedQuestion
                    {
                        Position = 1,
                        Question = new Question
                        {
                            Id = 1
                        }
                    }
                },
                {
                    new IndexedQuestion
                    {
                        Position = 2,
                        Question = new Question
                        {
                            Id = 2
                        }
                    }
                },
                {
                    new IndexedQuestion
                    {
                        Position = 3,
                        Question = new Question
                        {
                            Id = 3
                        }
                    }
                }
            };
            return questions;
        }

        /// <summary>
        /// Creates prototypes for creation.
        /// </summary>
        /// <returns>The prototypes.</returns>
        internal static IList<PrototypeCode> GetPrototypeCodes()
        {
            return new List<PrototypeCode>
            {
                new PrototypeCode
                {
                    Code = "newCode1",
                    PrototypesPerCode = 1
                },
                new PrototypeCode
                {
                    Code = "newCode3",
                    PrototypesPerCode = 3
                }
            };
        }
    }
}