﻿/*
*  Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Microsoft.Practices.Unity;
using Tholos.Security;
using Tholos.Services;

namespace Tholos.Host
{
    /// <summary>
    /// <para>
    /// This provides authorization access checking for service operations.
    /// </para>
    /// </summary>
    ///
    /// <remarks>
    /// <para>
    /// This extends <see cref="ServiceAuthorizationManager"/>.
    /// </para>
    /// </remarks>
    ///
    /// <author>veshu</author>
    /// 
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    public class CustomAuthorizationManager : ServiceAuthorizationManager
    {
        /// <summary>
        /// <para>
        /// Gets or sets the custom username and password validator.
        /// </para>
        /// </summary>
        /// <value>
        /// The custom username and password validator. It is not null after injection through the Unity.
        /// </value>
        [Dependency]
        public CustomPasswordValidator Validator { get; set; }


        /// <summary>
        /// <para>
        /// Initializes a new instance of the <see cref="CustomAuthorizationManager"/> class.
        /// </para>
        /// </summary>
        public CustomAuthorizationManager()
        {

        }

        /// <summary>
        /// Checks authorization for the given operation context based on default policy evaluation.
        /// This is used to authenticate using basic authentication header.
        /// </summary>
        /// <param name="operationContext">
        /// The System.ServiceModel.OperationContext for the current authorization request.
        /// </param>
        /// <returns>true if access is granted; otherwise, throws exception. The default is true.</returns>
        protected override bool CheckAccessCore(OperationContext operationContext)
        {
            //Extract the Authorization header, and parse out the credentials converting the Base64 string:
            if (WebOperationContext.Current != null)
            {
                var authHeader = WebOperationContext.Current.IncomingRequest.Headers["Authorization"];
                if (!string.IsNullOrEmpty(authHeader))
                {
                    var svcCredentials = Encoding.ASCII
                        .GetString(Convert.FromBase64String(authHeader.Substring(6)))
                        .Split(':');

                    var user = new { Username = svcCredentials[0], Password = svcCredentials[1] };

                    // authenticate, if fail it will throw exception.
                    Validator.Validate(user.Username, user.Password);

                    // set username which will be used by AuthroizationPolicy to create identity.
                    WebOperationContext.Current.IncomingRequest.Headers["X-UserName"] = user.Username;
                }
                else
                {
                    //No authorization header was provided, so challenge the client to provide before proceeding:
                    WebOperationContext.Current.OutgoingResponse.Headers.Add(
                        "WWW-Authenticate: Basic realm=\"TholosServices\"");
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;

                    var error = new ServiceFaultDetail
                    {
                        ErrorMessage = "Please provide a username and password",
                        ErrorType = "AuthenticationException"
                    };
                    throw new WebFaultException<ServiceFaultDetail>(error, HttpStatusCode.Unauthorized);
                }
            }
            return base.CheckAccessCore(operationContext);
        }

        /// <summary>
        /// <para>
        /// Checks whether this instance was properly configured.
        /// </para>
        /// </summary>
        ///
        /// <exception cref="ConfigurationException ">
        /// If it is not configured.
        /// </exception>
        public void CheckConfiguration()
        {
            if (Validator == null)
            {
                throw new ConfigurationException("Instance property 'Validator' wasn't configured properly.");
            }
        }
    }
}