﻿//
// Copyright (c) 2015, TopCoder, Inc. All rights reserved.
//

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Policy;
using System.ServiceModel;
using System.ServiceModel.Activation;
using log4net;
using log4net.Config;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Tholos.Security;

namespace Tholos.Host
{
    ///  <summary>
    ///  ServiceHostFactory to allow injecting a unity container.
    /// </summary>
    ///
    /// <threadsafety>
    ///  This class is immutable (assuming dependencies are not injected more than once).
    /// </threadsafety>
    /// 
    /// <author>veshu</author>
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    public class UnityServiceHostFactory : ServiceHostFactory
    {
        /// <summary>
        /// The _container
        /// </summary>
        private readonly IUnityContainer _container;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityServiceHostFactory"/> class.
        /// </summary>
        public UnityServiceHostFactory()
        {
            _container = new UnityContainer();
            var section = (UnityConfigurationSection)ConfigurationManager.GetSection("unity.services");
            section.Configure(_container);

            RegisterTypes(_container);
        }

        /// <summary>
        /// Creates a <see cref="T:System.ServiceModel.ServiceHost" /> for 
        /// a specified type of service with a specific base address.
        /// </summary>
        /// <param name="serviceType">Specifies the type of service to host.</param>
        /// <param name="baseAddresses">The <see cref="T:System.Array" /> of type
        ///  <see cref="T:System.Uri" /> that contains the base addresses 
        /// for the service hosted.</param>
        /// <returns>
        /// A <see cref="T:System.ServiceModel.ServiceHost" /> for the type of service 
        /// specified with a specific base address.
        /// </returns>
        protected override ServiceHost CreateServiceHost(
          Type serviceType, Uri[] baseAddresses)
        {
            var host = new UnityServiceHost(_container,
               serviceType, baseAddresses);
            host.Credentials.UserNameAuthentication.CustomUserNamePasswordValidator =
                _container.Resolve<CustomPasswordValidator>();
            var policies = new List<IAuthorizationPolicy>
            {
                _container.Resolve<AuthorizationPolicy>()
            };
            host.Authorization.ServiceAuthorizationManager = _container.Resolve<CustomAuthorizationManager>();
            host.Authorization.ExternalAuthorizationPolicies = policies.AsReadOnly();
            return host;
        }

        /// <summary>
        /// Registers the types.
        /// </summary>
        /// <param name="container">The container.</param>
        private void RegisterTypes(IUnityContainer container)
        {
            // Configure log4net.
            XmlConfigurator.Configure();

            // Register ILog.
            container.RegisterInstance(typeof(ILog), LogManager.GetLogger("TholosServices"));

        }
    }
}