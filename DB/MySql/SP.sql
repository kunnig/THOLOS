DROP PROCEDURE IF EXISTS  spCheckParticipantUsernamePassword;
DROP PROCEDURE IF EXISTS  spCheckUserUsernamePassword;
DROP PROCEDURE IF EXISTS  spGetUserByUsername;
DROP PROCEDURE IF EXISTS  spCreateProfileAnswer;
DROP PROCEDURE IF EXISTS  spGetParticipantRoles;
DROP PROCEDURE IF EXISTS  spGetUserRoles;
DROP PROCEDURE IF EXISTS  spGetUserRoleNames;

-- survey services
DROP PROCEDURE IF EXISTS spGetPrototypeTestsByParticipantSurveyId;
DROP PROCEDURE IF EXISTS spGetParticipantSurveysBySurveyId;
DROP PROCEDURE IF EXISTS spGetParticipantCredentials;
DROP PROCEDURE IF EXISTS spCreateQuickAccessSurvey;
DROP PROCEDURE IF EXISTS spDeleteQuickAccessSurveysByUserId;
DROP PROCEDURE IF EXISTS spGetQuickAccessSurveysByUserId;
DROP PROCEDURE IF EXISTS spGetUserSurveyStatus;
DROP PROCEDURE IF EXISTS spGetAllSurveys;
DROP PROCEDURE IF EXISTS spSearchSurveys;
DROP PROCEDURE IF EXISTS spGetPrototypeCodesBySurveyId;
DROP PROCEDURE IF EXISTS spGetSurveyPostTestingQuestionsBySurveyId;
DROP PROCEDURE IF EXISTS spGetSurveyPrototypeQuestionsBySurveyId;
DROP PROCEDURE IF EXISTS spGetSurvey;
DROP PROCEDURE IF EXISTS spDeletePrototypeCodesBySurveyId;
DROP PROCEDURE IF EXISTS spDeleteSurveyPostTestingQuestionsBySurveyId;
DROP PROCEDURE IF EXISTS spDeleteSurveyPrototypeQuestionsBySurveyId;
DROP PROCEDURE IF EXISTS spUpdateSurvey;
DROP PROCEDURE IF EXISTS spUpdateSurveyStatus;
DROP PROCEDURE IF EXISTS spCreateParticipantCredentials;
DROP PROCEDURE IF EXISTS spCreatePrototypeCode;
DROP PROCEDURE IF EXISTS spCreateSurveyPostTestingQuestion;
DROP PROCEDURE IF EXISTS spCreateSurveyPrototypeQuestion;
DROP PROCEDURE IF EXISTS spCreateSurvey;
DROP PROCEDURE IF EXISTS spGetAllPrototypeQuestions;
DROP PROCEDURE IF EXISTS spGetPrototypeAnswerOptionsByQuestionId;
DROP PROCEDURE IF EXISTS spGetAllPostTestingQuestions;
DROP PROCEDURE IF EXISTS spGetPostTestingAnswerOptionsByQuestionId;

-- participant services
DROP PROCEDURE IF EXISTS spGetProfileAnswerOptionsByQuestionId;
DROP PROCEDURE IF EXISTS spGetAllProfileQuestions;
DROP PROCEDURE IF EXISTS spGetParticipantIdByUsername;
DROP PROCEDURE IF EXISTS spUpdateParticipantCredentialsAsUsedById;
DROP PROCEDURE IF EXISTS spGetRoleIdByName;
DROP PROCEDURE IF EXISTS spCreateParticipant;
DROP PROCEDURE IF EXISTS spCreateParticipantRole;
DROP PROCEDURE IF EXISTS spGetParticipantCredentialsByUsername;
DROP PROCEDURE IF EXISTS spGetParticipant;
DROP PROCEDURE IF EXISTS spGetProfileAnswersByParticipantId;

--- participant survey services
DROP PROCEDURE IF EXISTS spGetSurveyPostTestingQuestionsByParticipantSurveyId;
DROP PROCEDURE IF EXISTS spGetSurveyPrototypeQuestionsByParticipantSurveyId;
DROP PROCEDURE IF EXISTS spGetSurveyPrototypeQuestionsForParticipantSurveyId;
DROP PROCEDURE IF EXISTS spIncreaseCompetedSurveysNumber;
DROP PROCEDURE IF EXISTS spCreatePostTestingAnswer;
DROP PROCEDURE IF EXISTS spUpdatePrototypeTestWithCompleteData;
DROP PROCEDURE IF EXISTS spGetParticipantSurveyByPrototypeTestId;
DROP PROCEDURE IF EXISTS spUpdatePrototypeAnswer;
DROP PROCEDURE IF EXISTS spCreatePrototypeAnswer;
DROP PROCEDURE IF EXISTS spGetPrototypeAnswersByPrototypeTestId;
DROP PROCEDURE IF EXISTS spGetPrototypeTest;
DROP PROCEDURE IF EXISTS spUpdateCurrentPrototypeTestId;
DROP PROCEDURE IF EXISTS spCreatePrototypeTest;
DROP PROCEDURE IF EXISTS spGetPostTestingAnswersByParticipantSurveyId;
DROP PROCEDURE IF EXISTS spGetPrototypeTestIdsByParticipantSurveyId;
DROP PROCEDURE IF EXISTS spGetParticipantSurvey;

/**************************************************************************
** Stored Procedure: spGetSurveyPrototypeQuestionsBySurveyId
**
** In:
**    SurveyId	-	 The  Id
**
** Description: 
**   Gets survey prototype questions by survey id
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetSurveyPrototypeQuestionsBySurveyId
	(IN SurveyId	BIGINT)      
BEGIN 
	SELECT pq.Id, Text, Conditional, QuestionType, QuestionNumber, pqc.Id as ConditionId, 
	ConditionalOnQuestionId, ConditionalOnAnswerId
	FROM `PrototypeQuestion` pq
	LEFT JOIN `PrototypeQuestionCondition` pqc ON pq.Id=pqc.QuestionId
	INNER JOIN `SurveyProrotypeQuestion` spq ON spq.QuestionId=pq.Id
	WHERE spq.`SurveyId`=SurveyId;
END$$

/**************************************************************************
** Stored Procedure: spGetSurveyPostTestingQuestionsByParticipantSurveyId
**
** In:
**    ParticipantSurveyId	-	 The Participant Survey Id
**
** Description: 
**   Gets survey PostTesting questions by Participant Survey id
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetSurveyPostTestingQuestionsByParticipantSurveyId
	 (IN ParticipantSurveyId	BIGINT) 
BEGIN
	SELECT pq.Id, Text, Conditional, QuestionType, QuestionNumber, pqc.Id as ConditionId, 
	ConditionalOnQuestionId, ConditionalOnAnswerId
	FROM PostTestingQuestion pq
	LEFT JOIN PostTestingQuestionCondition pqc ON pq.Id=pqc.QuestionId
	INNER JOIN SurveyPostTestingQuestion spq ON spq.QuestionId=pq.Id
	INNER JOIN ParticipantSurvey ps ON ps.SurveyId=spq.SurveyId
	WHERE ps.Id=ParticipantSurveyId;
END$$

/**************************************************************************
** Stored Procedure: spGetSurveyPrototypeQuestionsByParticipantSurveyId
**
** In:
**    ParticipantSurveyId	-	 The Participant Survey Id
**
** Description: 
**   Gets survey prototype questions by Participant Survey id
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetSurveyPrototypeQuestionsByParticipantSurveyId
	 (IN ParticipantSurveyId	BIGINT)      
BEGIN   
	SELECT pq.Id, Text, Conditional, QuestionType, QuestionNumber, pqc.Id as ConditionId, 
	ConditionalOnQuestionId, ConditionalOnAnswerId
	FROM `PrototypeQuestion` pq
	LEFT JOIN `PrototypeQuestionCondition` pqc ON pq.Id=pqc.QuestionId
	INNER JOIN `SurveyProrotypeQuestion` spq ON spq.QuestionId=pq.Id
	INNER JOIN `ParticipantSurvey` ps ON ps.SurveyId=spq.SurveyId
	WHERE ps.Id=ParticipantSurveyId;
END$$
DELIMITER ;
DELIMITER $$
/**************************************************************************
 ** Stored Procedure: spIncreaseCompetedSurveysNumber
 ** In:
 **    ParticipantSurveyId - the Participant Survey Id .
 ** Out:
 **    Id - The Id
 ** Description:
 **    Increases the completed surveys number.
 **************************************************************************/
CREATE PROCEDURE spIncreaseCompetedSurveysNumber
	(IN ParticipantSurveyId BIGINT)
BEGIN
	UPDATE `Survey` s
	INNER JOIN `ParticipantSurvey` ps ON ps.SurveyId=s.Id
    SET CompletedSurveysNumber= CompletedSurveysNumber + 1
	WHERE ps.Id=ParticipantSurveyId;
END$$
DELIMITER ;

/**************************************************************************
 ** Stored Procedure: spCreatePostTestingAnswer
 ** In:
 **    ParticipantSurveyId - the Participant Survey Id .
 **    AnswerType - the answer type.
 **    QuestionId - the question id.
 **    AnswerOptionId - the answer option id.
 **    Input - the input.
 **    Value - the value.
 ** Out:
 **    Id - The Id
 ** Description:
 **    Inserts a new record into the PostTestingAnswer table and child tables depending on the AnswerType.
 **************************************************************************/
DELIMITER $$
CREATE PROCEDURE spCreatePostTestingAnswer
(OUT Id	BIGINT,
IN ParticipantSurveyId BIGINT,
IN AnswerType VARCHAR(32),
IN QuestionId BIGINT,
IN AnswerOptionId BIGINT ,
IN Input VARCHAR(128),
IN Value INT)
BEGIN
	INSERT INTO `PostTestingAnswer`(QuestionId, AnswerType, ParticipantSurveyId)
	VALUES(QuestionId,AnswerType,ParticipantSurveyId);
	SET Id=LAST_INSERT_ID();
	
    IF (AnswerType='RangeAnswer') THEN
		INSERT INTO `PostTestingRangeAnswer`(Id, Value)
		VALUES(Id,Value);
	ELSE 
			INSERT INTO `PostTestingSingleAnswer`(Id, AnswerOptionId)
			VALUES(Id,AnswerOptionId);
		IF (AnswerType='SingleAnswerInput') THEN
			INSERT INTO `PostTestingSingleInputAnswer`(Id, Input)
			VALUES(Id, Input);
		END IF;
	END IF;
	
END$$
DELIMITER ;

/**************************************************************************
** Stored Procedure: spUpdatePrototypeTestWithCompleteData
**
** In:
**   Id - The Id
**	 AfterWeight - The after weight 
**	 AfterPhotoLocalPath - The  after photo local path
**	 AfterPhotoUri - the after photo uri
** 
** Description: 
**   Inserts into prototype test table
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spUpdatePrototypeTestWithCompleteData
	(IN Id					BIGINT,
	IN AfterWeight			REAL,
	IN AfterPhotoLocalPath	VARCHAR(1024),
	IN AfterPhotoUri VARCHAR(1024))
BEGIN
	UPDATE `PrototypeTest` AS pt
	SET pt.AfterWeight = AfterWeight,
		pt.AfterPhotoLocalPath = AfterPhotoLocalPath,
		pt.AfterPhotoUri=AfterPhotoUri,
		pt.Completed = 1
	WHERE pt.Id = Id;
	UPDATE `ParticipantSurvey` AS ps
	SET ps.CurrentPrototypeTestId = 0
	WHERE ps.CurrentPrototypeTestId = Id;
END$$

/**************************************************************************
** Stored Procedure: spGetParticipantSurveyByPrototypeTestId
**
** In:
**	  PrototypeTestId - The PrototypeTest Id
**
** Description: 
**   Gets the participant survey fields by PrototypeTest Id.
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetParticipantSurveyByPrototypeTestId
	(IN PrototypeTestId	 BIGINT	)
BEGIN
	SELECT ps.Id 
      , ParticipantId 
      , SurveyId 
      , CurrentPrototypeTestId 
	FROM  ParticipantSurvey  ps
	INNER JOIN  PrototypeTest  pt ON ps.Id=pt.ParticipantSurveyId
	WHERE pt.Id=PrototypeTestId;
END$$

/**************************************************************************
 ** Stored Procedure: spUpdatePrototypeAnswer
 ** In:
 **    AnswerType - the answer type.
 **    AnswerOptionId - the answer option id.
 **    Input - the input.
 **    Value - the value.
 ** Out:
 **    Id - The Id
 ** Description:
 **    Updates the PrototypeAnswer table and child tables depending on the AnswerType.
 **************************************************************************/
DELIMITER $$
CREATE PROCEDURE spUpdatePrototypeAnswer
	(IN Id	BIGINT,
	IN AnswerType VARCHAR(32),
	IN AnswerOptionId BIGINT,
	IN Input VARCHAR(128),
	IN Value INT)
BEGIN
	
	IF (AnswerType='RangeAnswer') THEN 
			UPDATE PrototypeRangeAnswer pra
			SET pra.Value=Value
			WHERE pra.Value=Id;
	ELSEIF (AnswerType='SingleAnswerInput' OR AnswerType='SingleAnswer') THEN
	
			UPDATE PrototypeSingleAnswer psa
			SET psa.AnswerOptionId=AnswerOptionId
			WHERE psa.Id=Id;
            
			IF (AnswerType='SingleAnswerInput') THEN
				UPDATE PrototypeSingleInputAnswer psia
				SET psia.Input=Input
				WHERE psia.Id=Id;
			END IF;
	END IF;
END$$

/**************************************************************************
 ** Stored Procedure: spCreatePrototypeAnswer
 ** In:
 **    PrototypeTestId - the profile answer id.
 **    AnswerType - the answer type.
 **    QuestionId - the question id.
 **    AnswerOptionId - the answer option id.
 **    Input - the input.
 **    Value - the value.
 ** Out:
 **    Id - The Id
 ** Description:
 **    Inserts a new record into the PrototypeAnswer table and child tables depending on the AnswerType.
 **************************************************************************/
DELIMITER $$
CREATE PROCEDURE spCreatePrototypeAnswer
(OUT Id	BIGINT,
IN PrototypeTestId BIGINT,
IN AnswerType VARCHAR(32),
IN QuestionId BIGINT,
IN AnswerOptionId BIGINT,
IN Input VARCHAR(128),
IN Value INT)
BEGIN
	INSERT INTO PrototypeAnswer(QuestionId, AnswerType, PrototypeTestId)
	VALUES( QuestionId,AnswerType,PrototypeTestId);
	SET Id=LAST_INSERT_ID();
	
	IF (AnswerType='RangeAnswer') THEN
		INSERT INTO PrototypeRangeAnswer(Id, Value)
		VALUES(Id,value);
	ELSEIF (AnswerType='SingleAnswerInput' OR AnswerType='SingleAnswer') THEN
			INSERT INTO PrototypeSingleAnswer(Id, AnswerOptionId)
			VALUES(Id,AnswerOptionId);

		IF (AnswerType='SingleAnswerInput') THEN
			INSERT INTO PrototypeSingleInputAnswer(Id, Input)
			VALUES(Id, Input);
		END IF;
	END IF;
END$$

/**************************************************************************
** Stored Procedure: spGetPrototypeAnswersByPrototypeTestId
**
** In:
**	 @PrototypeTestId - The Prototype Test Id
**
** Description: 
**   Gets prototype answers by prototype test id.
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetPrototypeAnswersByPrototypeTestId
	(IN PrototypeTestId BIGINT)
BEGIN 
	SELECT pa.Id, QuestionId, AnswerType, pra.Value, psa.AnswerOptionId, psia.Input
	FROM PrototypeAnswer pa
	LEFT JOIN PrototypeRangeAnswer pra ON pa.Id = pra.Id
	LEFT JOIN PrototypeSingleAnswer psa ON pa.Id= psa.Id
	LEFT JOIN PrototypeSingleInputAnswer psia ON psia.Id = pa.Id
	WHERE pa.PrototypeTestId=PrototypeTestId; 
END$$

/**************************************************************************
** Stored Procedure: spGetPrototypeTest
**
** In:
**	 @PrototypeTestId - The Prototype Test Id
**
** Description: 
**   Gets prototype test fields
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetPrototypeTest
	(IN PrototypeTestId BIGINT)
BEGIN 
	SELECT Id
		  ,PrototypeCode
		  ,Iteration
		  ,BeforeWeight
		  ,AfterWeight
		  ,BeforePhotoUri
		  ,AfterPhotoUri
          , BeforePhotoLocalPath
          , AfterPhotoLocalPath
          , Completed
	   FROM  PrototypeTest
       WHERE Id = PrototypeTestId;
END$$

/**************************************************************************
** Stored Procedure: spUpdateCurrentPrototypeTestId
**
** In:
**	 @ParticipantSurveyId - The Participant Survey Id
**	 @PrototypeTestId - The Prototype Test Id
**
** Description: 
**   Inserts into prototype test table
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spUpdateCurrentPrototypeTestId
	(IN ParticipantSurveyId BIGINT ,
	IN PrototypeTestId BIGINT)
BEGIN
	UPDATE ParticipantSurvey
	SET CurrentPrototypeTestId = PrototypeTestId
	WHERE Id = ParticipantSurveyId;
END$$

/**************************************************************************
** Stored Procedure: spCreatePrototypeTest
**
** In:
**    PrototypeCode - The Prototype Code
**	  ParticipantSurveyId - The Participant Survey Id
**    Iteration - the iteration
**	  BeforePhotoLocalPath - The  Before photo local path
**	  BeforePhotoUri - the before photo uri
** Out:
**     Id - The Id
**
** Description: 
**   Inserts into prototype test table
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spCreatePrototypeTest
	(IN PrototypeCode  VARCHAR(64),
	IN ParticipantSurveyId	 BIGINT	,
	IN Iteration INT,
	IN BeforePhotoLocalPath VARCHAR(1024),
	IN BeforePhotoUri VARCHAR(1024),
	OUT Id	BIGINT)
BEGIN 
	DECLARE Weight INT;
	SELECT p.Weight INTO Weight FROM Participant p INNER JOIN ParticipantSurvey ps ON p.Id = ps.ParticipantId WHERE ps.Id = ParticipantSurveyId;
	INSERT INTO   PrototypeTest ( ParticipantSurveyId ,  PrototypeCode , Iteration ,  BeforeWeight ,  BeforePhotoLocalPath , BeforePhotoUri )
    VALUES( ParticipantSurveyId,  PrototypeCode, Iteration,  Weight,  BeforePhotoLocalPath, BeforePhotoUri);

	SET  Id =  LAST_INSERT_ID();
	UPDATE ParticipantSurvey ps SET CurrentPrototypeTestId = Id WHERE ps.Id = ParticipantSurveyId;
END$$

/**************************************************************************
** Stored Procedure: spGetPostTestingAnswersByParticipantSurveyId
**
** In:
**	  ParticipantSurveyId - The Participant Survey Id
**
** Description: 
**   Gets the id's from prototype test table
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetPostTestingAnswersByParticipantSurveyId
	(IN ParticipantSurveyId	 BIGINT	)
BEGIN 
  SELECT pra.Id, QuestionId, AnswerType, pra.Value, psa.AnswerOptionId, psia.Input
	FROM  PostTestingAnswer AS pa
	LEFT JOIN  PostTestingRangeAnswer  pra ON pa.Id = pra.Id
	LEFT JOIN  PostTestingSingleAnswer  psa ON pa.Id= psa.Id
	LEFT JOIN  PostTestingSingleInputAnswer  psia ON psia.Id = pa.Id
	WHERE pa.ParticipantSurveyId= ParticipantSurveyId; 
END$$


/**************************************************************************
** Stored Procedure: spGetPrototypeTestIdsByParticipantSurveyId
**
** In:
**	   ParticipantSurveyId - The participant survey Id
**
** Description: 
**   Gets the participant survey fields
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetPrototypeTestIdsByParticipantSurveyId
	(IN ParticipantSurveyId	 BIGINT	)
BEGIN   
	SELECT  Id 
	FROM PrototypeTest AS pt
	WHERE pt.ParticipantSurveyId = ParticipantSurveyId;
END$$


/**************************************************************************
** Stored Procedure: spGetParticipantSurvey
**
** In:
**	   ParticipantSurveyId - The participant survey Id
**
** Description: 
**   Gets the participant survey fields
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetParticipantSurvey
	(IN ParticipantSurveyId	 BIGINT	)
BEGIN     
	SELECT  Id 
      , ParticipantId 
      , SurveyId 
      , CurrentPrototypeTestId 
	FROM  ParticipantSurvey 
	WHERE   Id = ParticipantSurveyId;
END$$
/**************************************************************************
** Stored Procedure: spGetPrototypeTestsByParticipantSurveyId
**
** In:
**	  ParticipantSurveyId - The participant survey Id
**
** Description: 
**   Gets the participant survey fields
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetPrototypeTestsByParticipantSurveyId
	(IN ParticipantSurveyId	 BIGINT	)
BEGIN 
	SELECT  Id 
		  , PrototypeCode 
		  , Iteration 
		  , BeforeWeight 
		  , AfterWeight 
		  , BeforePhotoUri 
		  , AfterPhotoUri 
          , BeforePhotoLocalPath
          , AfterPhotoLocalPath
          , Completed
	FROM  PrototypeTest AS pt
	WHERE  pt.ParticipantSurveyId = ParticipantSurveyId;
END$$
/**************************************************************************
** Stored Procedure: spGetParticipantSurveysBySurveyId
**
** In:
**	   SurveyId - The Survey Id
**
** Description: 
**   Gets the participant survey fields
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetParticipantSurveysBySurveyId
	(IN SurveyId	 BIGINT	)
BEGIN   
	SELECT  Id 
      , ParticipantId 
      , SurveyId 
      , CurrentPrototypeTestId 
	FROM  ParticipantSurvey AS ps
	WHERE   ps.SurveyId = SurveyId;
END$$


/**************************************************************************
** Stored Procedure: spGetParticipantCredentials
**
** In:
**	   SurveyId - The Survey Id
**
** Description: 
**   Gets the participant credentails for a survey.
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetParticipantCredentials
	(IN SurveyId	 BIGINT	)
BEGIN 
	SELECT  Id ,
		  SurveyId ,
		  Username ,
		  Password ,
          Used
	FROM  ParticipantCredentials pc
	WHERE  pc.SurveyId = SurveyId;
END$$

/**************************************************************************
** Stored Procedure: spCreateQuickAccessSurvey
**
** In:
**	   SurveyId - The Survey Id
**     UserId	- The  User Id
**	   Position - The Position
**
** Description: Used
**   Inserts quick access survey data
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spCreateQuickAccessSurvey
	(IN SurveyId	 BIGINT	,
	UserId		 BIGINT,
	Position	 INT,
	OUT Id		 BIGINT )
BEGIN  
	
	INSERT INTO QuickAccessSurvey ( UserId ,  SurveyId ,  Position )
    VALUES( UserId,  SurveyId,  Position);

	SET  Id =  LAST_INSERT_ID();
END$$

/**************************************************************************
** Stored Procedure: spDeleteQuickAccessSurveysByUserId
**
** In:
**     UserId	-	 The  User Id
**
** Description: 
**   Removes quick access survey fields for given user id
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spDeleteQuickAccessSurveysByUserId
	  (IN UserId BIGINT)
BEGIN   
	DELETE FROM quickaccess
    USING  QuickAccessSurvey AS quickaccess
    WHERE  quickaccess.`UserId` = UserId;
END$$

/**************************************************************************
** Stored Procedure: spGetQuickAccessSurveysByUserId
**
** In:
**     UserId	-	 The  User Id
**
** Description: 
**   Gets quick access survey fields and survery fields by user id
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetQuickAccessSurveysByUserId
	  (IN UserId BIGINT)
BEGIN    
	SELECT qas.SurveyId ,s.Name ,s.CellName , qas.Position       
	FROM  QuickAccessSurvey AS qas 
	INNER JOIN  Survey  s ON qas.SurveyId =s.Id 
	WHERE  qas.UserId =  UserId;
END$$
/**************************************************************************
** Stored Procedure: spGetUserSurveyStatus
**
** In:
**     UserId	-	 The  User Id
**
** Description: 
**   Gets survey status
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetUserSurveyStatus
	   (IN UserId BIGINT)
BEGIN 
	SELECT  Status       
	FROM  Survey  	
	WHERE  CreatedBy =  UserId;
END$$
/**************************************************************************
** Stored Procedure: spGetPrototypeCodesBySurveyId
**
** In:
**    SurveyId	-	 The  Survey Id
**    Status	-	 The expected survey status
**
** Description: 
**   Gets prototype codes by survey id
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetPrototypeCodesBySurveyId
	 (IN SurveyId	BIGINT,
	  IN Status		VARCHAR(32))
BEGIN 	 	
     SELECT pc.Id, pc.Code, pc.PrototypesPerCode
	 FROM PrototypeCode	AS pc JOIN Survey AS s ON pc.SurveyId = s.Id
	 WHERE pc.SurveyId=SurveyId
	 AND (Status IS NULL OR Status = '' OR s.Status = Status);
END$$


/**************************************************************************
** Stored Procedure: spGetSurveyPostTestingQuestionsBySurveyId
**
** In:
**    SurveyId	-	 The  Survey Id
**
** Description: 
**   Gets survey post testing questions by survey id
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetSurveyPostTestingQuestionsBySurveyId
	  (IN SurveyId		 BIGINT)  
BEGIN 	 	
    SELECT ptq.Id, Text, Conditional, QuestionType, QuestionNumber, ptqc.Id as ConditionId, 
		ConditionalOnQuestionId, ConditionalOnAnswerId
	FROM PostTestingQuestion ptq
		LEFT JOIN PostTestingQuestionCondition ptqc ON ptq.Id=ptqc.QuestionId
		INNER JOIN	 SurveyPostTestingQuestion sptq	ON sptq.QuestionId= ptq.Id	
	WHERE sptq.SurveyId=SurveyId;
 END$$

/**************************************************************************
** Stored Procedure: spGetSurvey
**
** In:
**    SurveyId	-	 The Id
**
** Description: 
**   Gets survey fields
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetSurvey
	   (IN SurveyId		 BIGINT)   
BEGIN 	 	
    SELECT Id ,Name, CellName, ParticipantsNumber, Status, DateCreated, CompletedSurveysNumber
		,Draft, CreatedBy
	FROM Survey 
	WHERE Id = SurveyId;
 END$$

/**************************************************************************
** Stored Procedure: spDeletePrototypeCodesBySurveyId
**
** In:
**    SurveyId	-	The  Survey Id
**
** Description: 
**   Deletes prototype codes by surveyd id
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spDeletePrototypeCodesBySurveyId
	   (IN SurveyId		 BIGINT)      
BEGIN 	 	
	DELETE FROM pc
	USING PrototypeCode AS pc
	WHERE pc.SurveyId= SurveyId; 	
 END$$

/**************************************************************************
** Stored Procedure: spDeleteSurveyPostTestingQuestionsBySurveyId
**
** In:
**    SurveyId	-	 The  Survey Id
**
** Description: 
**   Deletes survey post testing questions by surveyd id
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spDeleteSurveyPostTestingQuestionsBySurveyId
	  (IN SurveyId		 BIGINT)
BEGIN 	 	
     DELETE FROM sptq
     USING SurveyPostTestingQuestion AS sptq
	 WHERE sptq.SurveyId= SurveyId;  	
END$$

/**************************************************************************
** Stored Procedure: spDeleteSurveyPrototypeQuestionsBySurveyId
**
** In:
**    SurveyId	-	 The  Survey Id
**
** Description: 
**   Deletes survey prototype questions by survey id
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spDeleteSurveyPrototypeQuestionsBySurveyId
	  (IN SurveyId	BIGINT)   
BEGIN 	 	
    DELETE FROM spq 
    USING SurveyProrotypeQuestion AS spq
	WHERE spq.SurveyId= SurveyId;	
 END$$
/**************************************************************************
** Stored Procedure: spUpdateSurvey
**
** In:
**    Id		-				 The  Id
**    Name		-				 The  Name
**    CellName -				 The Cell Name
**    Status	-				 The Status
**    CompletedSurveysNumber -	 The Completed Survery Number
**    Draft	 -				 The Draft
**
** Description: 
**   Updates the survey
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spUpdateSurvey
	(IN Id						BIGINT,
     IN  Name					VARCHAR(64),
     IN  CellName				VARCHAR(64),
     IN  Status					 VARCHAR(32),
     IN  CompletedSurveysNumber	 INT,
     IN  Draft					 BIT)
  BEGIN 	  	
	UPDATE Survey AS s
		SET  s.Name = Name
			,s.CellName = CellName			
			,s.Status = Status
			,s.CompletedSurveysNumber = CompletedSurveysNumber
			,s.Draft = Draft		
	WHERE s.Id= Id;
 END$$
/**************************************************************************
** Stored Procedure: spUpdateSurveyStatus
**
** In:
**    Id		-		 The  Id
**    Status	-		 The Status
**    Draft	-		 The Draft
**
** Description: 
**   Updates survey status
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spUpdateSurveyStatus
	(IN Id BIGINT,
     IN Status	 VARCHAR(32),
     IN  Draft	 BIT)
BEGIN 	  	     
	UPDATE Survey AS s
	SET s.Status = Status,
		s.Draft = Draft	
	WHERE s.Id= Id;
END$$

/**************************************************************************
 ** Stored Procedure: spCreateParticipantCredentials
 **
 ** In:
 **   SurveyId	 - The Survey Id
 **   Username	- The User Name
 **   Password - The Password
 ** Out:
 **    Id - The Id
 **
 ** Description:
 **    Creates participant's credentials
 **************************************************************************/
DELIMITER $$
CREATE PROCEDURE spCreateParticipantCredentials	
	(IN SurveyId BIGINT,
    IN Username VARCHAR(64),
	IN Password VARCHAR(64),	
   	OUT Id	BIGINT)
BEGIN
	
	INSERT INTO ParticipantCredentials (SurveyId, Username, Password, Used)          
    VALUES(SurveyId, Username, Password, 0);

	SET Id = LAST_INSERT_ID();
END$$
/**************************************************************************
 ** Stored Procedure: spCreatePrototypeCode
 **
 ** In:
 **    Code	 - The Code
 **    PrototypesPerCode	- Prototypes Per Code
 **    SurveyId - The Survery Id
 ** Out:
 **    Id - The Id
 **
 ** Description:
 **    Creates survey
 **************************************************************************/
DELIMITER $$
CREATE PROCEDURE spCreatePrototypeCode	
	(IN Code						VARCHAR(64),
	IN PrototypesPerCode			VARCHAR(64),   
    IN SurveyId					BIGINT,
   	OUT Id							BIGINT)
BEGIN
	INSERT INTO PrototypeCode (Code ,PrototypesPerCode, SurveyId)
    VALUES (Code, PrototypesPerCode, SurveyId);

	SET Id = LAST_INSERT_ID();
END$$
/**************************************************************************
 ** Stored Procedure: spCreateSurveyPostTestingQuestion
 **
 ** In:
 **   SurveyId	 - The Survey Id
 **   QuestionId	- Question Id
 **   QuestionNumber - Question Number
 ** Out:
 **    Id - The Id
 **
 ** Description:
 **    Creates survey post testing question
 **************************************************************************/
DELIMITER $$
CREATE PROCEDURE spCreateSurveyPostTestingQuestion	
	(IN SurveyId BIGINT,
    IN QuestionId BIGINT,
	IN QuestionNumber INT,
   	OUT Id	BIGINT)
BEGIN
	INSERT INTO SurveyPostTestingQuestion(SurveyId, QuestionId, QuestionNumber)
    VALUES ( SurveyId, QuestionId, QuestionNumber);

	SET Id = LAST_INSERT_ID();
END$$
/**************************************************************************
 ** Stored Procedure: spCreateSurveyPrototypeQuestion
 **
 ** In:
 **   SurveyId	 - The Survey Id
 **   QuestionId	- Question Id
 **   QuestionNumber - The QuestionNumber
 ** Out:
 **    Id - The Id
 **
 ** Description:
 **    Creates survey prototype question
 **************************************************************************/
DELIMITER $$
CREATE PROCEDURE spCreateSurveyPrototypeQuestion	
	(IN SurveyId BIGINT,
    IN QuestionId BIGINT,
    IN QuestionNumber INT,
   	OUT Id	BIGINT)
BEGIN
	INSERT INTO SurveyProrotypeQuestion(SurveyId, QuestionId, QuestionNumber)
    VALUES (SurveyId, QuestionId, QuestionNumber);

	SET Id = LAST_INSERT_ID();
END$$
/**************************************************************************
 ** Stored Procedure: spCreateSurvey
 **
 ** In:
 **    Name - The Name
 **    CellName- The Cell Name
 **    ParticipantsNumber - Participants Number
 **    Status - The Status.
 **    CompletedSurveysNumber - Completed Surveys Number
 **    Draft - The Draft
 **    CreatedBy - Created By
 ** Out:
 **    Id - The Id
 **
 ** Description:
 **    Creates survey
 **************************************************************************/
DELIMITER $$
CREATE PROCEDURE spCreateSurvey	
	(IN Name						VARCHAR(64),
	IN CellName					VARCHAR(64),
	IN  ParticipantsNumber			INT,
	IN  Status						VARCHAR(32),
	IN CompletedSurveysNumber		INT,
	IN  Draft						BIT,
	IN  CreatedBy					BIGINT,
	OUT Id							BIGINT)
BEGIN
	INSERT INTO Survey (Name,CellName,ParticipantsNumber,Status,CompletedSurveysNumber,Draft,CreatedBy,DateCreated)
	VALUES(Name, CellName, ParticipantsNumber, Status, CompletedSurveysNumber,Draft, CreatedBy,NOW());

	SET Id=LAST_INSERT_ID();
END$$

/**************************************************************************
** Stored Procedure: spGetPrototypeAnswerOptionsByQuestionId
**
** Description: 
**   Gets all Prototype question answer options
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetPrototypeAnswerOptionsByQuestionId 
	(IN QuestionId BIGINT)
BEGIN 
	SELECT pa.Id, QuestionId, AnswerType, Value, Label, FromValueLabel, ToValueLabel, FromValue, ToValue, Increment
	FROM PrototypeAnswerOption pa
	LEFT JOIN PrototypeRangeAnswerOption pra ON pa.Id = pra.Id
	LEFT JOIN PrototypeSingleAnswerOption psa ON pa.Id= psa.Id
	WHERE pa.QuestionId=QuestionId; 
END$$

/**************************************************************************
** Stored Procedure: spGetAllPrototypeQuestions
**
** Description: 
**   Gets all Prototype questions
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetAllPrototypeQuestions()
BEGIN 
	SELECT pq.Id, Text, Conditional, QuestionType, pqc.Id as ConditionId, 
	ConditionalOnQuestionId, ConditionalOnAnswerId
	FROM PrototypeQuestion pq
	LEFT JOIN PrototypeQuestionCondition pqc ON pq.Id=pqc.QuestionId;
END$$

/**************************************************************************
** Stored Procedure: spGetPostTestingAnswerOptionsByQuestionId
**
** Description: 
**   Gets all PostTesting question answer options
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetPostTestingAnswerOptionsByQuestionId 
	(IN QuestionId BIGINT)
BEGIN 
	SELECT pa.Id, QuestionId, AnswerType, Value, Label, FromValueLabel, ToValueLabel, FromValue, ToValue, Increment
	FROM PostTestingAnswerOption pa
	LEFT JOIN PostTestingRangeAnswerOption pra ON pa.Id = pra.Id
	LEFT JOIN PostTestingSingleAnswerOption psa ON pa.Id= psa.Id
	WHERE pa.QuestionId=QuestionId; 
END$$

/**************************************************************************
** Stored Procedure: spGetAllPostTestingQuestions
**
** Description: 
**   Gets all PostTesting questions
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetAllPostTestingQuestions()
BEGIN 
	SELECT pq.Id, Text, Conditional, QuestionType, pqc.Id as ConditionId, 
	ConditionalOnQuestionId, ConditionalOnAnswerId
	FROM PostTestingQuestion pq
	LEFT JOIN PostTestingQuestionCondition pqc ON pq.Id=pqc.QuestionId;
END$$


/**************************************************************************
** Stored Procedure: spGetProfileAnswerOptionsByQuestionId
**
** Description: 
**   Gets all profile question answer options
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetProfileAnswerOptionsByQuestionId 
	(IN QuestionId BIGINT)
BEGIN 
	SELECT pa.Id, QuestionId, AnswerType, Value, Label, FromValueLabel, ToValueLabel, FromValue, ToValue, Increment
	FROM ProfileAnswerOption pa
	LEFT JOIN ProfileRangeAnswerOption pra ON pa.Id = pra.Id
	LEFT JOIN ProfileSingleAnswerOption psa ON pa.Id= psa.Id
	WHERE pa.QuestionId=QuestionId; 
END$$

/**************************************************************************
** Stored Procedure: spGetAllProfileQuestions
**
** Description: 
**   Gets all profile questions
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetAllProfileQuestions()
BEGIN 
	SELECT pq.Id, Text, Conditional, QuestionType, pqc.Id as ConditionId, 
	ConditionalOnQuestionId, ConditionalOnAnswerId
	FROM ProfileQuestion pq
	LEFT JOIN ProfileQuestionCondition pqc ON pq.Id=pqc.QuestionId;

END$$

/**************************************************************************
** Stored Procedure: spGetParticipantIdByUsername
**
** In:
**   Username - The user name
**
** Description: 
**   Gets participant id by user name
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetParticipantIdByUsername
(IN Username VARCHAR(64))
BEGIN 
	SELECT Id
	FROM Participant p
	WHERE p.Username=Username;
END$$

/**************************************************************************
** Stored Procedure: spUpdateParticipantCredentialsAsUsedById
**
** In:
**   Username - The usern name
**
** Description: 
		Updates participant credential BEGIN used by id
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spUpdateParticipantCredentialsAsUsedById
(IN Id BIGINT)
BEGIN 
	UPDATE ParticipantCredentials AS pc
	SET  pc.Used = 1
	WHERE pc.Id=Id;
END$$
/**************************************************************************
** Stored Procedure: spGetRoleIdByName
**
** In:
**   Name - The role name
**
** Description: 
		Selects the role id by name
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetRoleIdByName
(IN Name VARCHAR(64))
BEGIN 
	SELECT r.Id, r.Name
    FROM Role r
	WHERE  r.Name=Name;
END$$


/**************************************************************************
** Stored Procedure: spCreateParticipant
**
** In:
**   Username - The user name
**	 SurveyId - The survery id
**	 BirthDate - the birth date
**	 Weight - The weight
**	 HeightFeet - The height in feet
**	 HeightInches - The height in inches
 ** Out:
 **    Id - The Id
** Description: 
**		Creates participant 
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spCreateParticipant
(OUT Id BIGINT,
IN Username VARCHAR(45),
IN SurveyId BIGINT,
IN Weight INT,
IN HeightFeet INT,
IN HeightInches INT,
IN BirthDate DATETIME)
BEGIN 
	INSERT INTO Participant(Username,SurveyId,BirthDate,Weight,HeightFeet,HeightInches)
    VALUES (Username, SurveyId, BirthDate, Weight, HeightFeet, HeightInches);

	SET Id= LAST_INSERT_ID();
	INSERT INTO ParticipantSurvey(Id,ParticipantId,SurveyId,CurrentPrototypeTestId)
	VALUES (Id, Id, SurveyId, 0);
END$$

/**************************************************************************
** Stored Procedure: spCreateParticipantRole
**
** In:
**   Username - The user name
**	 RoleId - The role id
**
** Description: 
		Creates participant role
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spCreateParticipantRole
	(IN ParticipantId			BIGINT,
	IN RoleId					BIGINT)
BEGIN 
	INSERT INTO ParticipantRole(ParticipantId,RoleId)
    VALUES (ParticipantId, RoleId);
END$$
/**************************************************************************
** Stored Procedure: spGetParticipantCredentialsByUsername
**
** In:
**   Username - The user name
**
** Description: 
		Selects participant credentials by username.
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetParticipantCredentialsByUsername
(IN Username VARCHAR(64))		
BEGIN 
	SELECT pc.Id,
		 pc.SurveyId,
		 pc.Username,
		 pc.Password,
         pc.Used
	  FROM ParticipantCredentials AS pc
	  WHERE	pc.Username=Username AND pc.Used=0 LIMIT 1;
END$$

/**************************************************************************
** Stored Procedure: spGetParticipant
**
** In:
**   Participantd - The participant id
**
** Description: 
**   Gets participants
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetParticipant
(IN ParticipantId	BIGINT)
BEGIN 
	SELECT Id,
      Username,
      SurveyId,
	  BirthDate,
      Weight,
      HeightFeet,
      HeightInches
	FROM Participant
	WHERE Id=ParticipantId;
END$$
/**************************************************************************
** Stored Procedure: spGetUserRoles
**
** In:
**   Username - The user name
**
** Description: 
**   Gets user roles
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetUserRoles
(IN Username VARCHAR(64))
BEGIN
	SELECT r.Id, r.Name
	FROM Role AS r
	INNER JOIN UserRole ur ON r.Id=ur.RoleId
	INNER JOIN Users AS u  ON u.Id=ur.UserId
	WHERE u.Username=Username;
END$$
/**************************************************************************
** Stored Procedure: spGetUserRoleNames
**
** In:
**   Username - The user name
**
** Description: 
**   Gets user role names
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetUserRoleNames
(IN Username VARCHAR(64))
BEGIN
	SELECT r.Name
	FROM Role AS r
	INNER JOIN UserRole ur ON r.Id=ur.RoleId
	INNER JOIN Users AS u  ON u.Id=ur.UserId
	WHERE u.Username=Username;
END$$
/**************************************************************************
** Stored Procedure: spGetParticipantRoles
**
** In:
**   Username - The user name
**
** Description: 
**   Gets participant roles
**************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetParticipantRoles
(IN username VARCHAR(64))
BEGIN
	SELECT r.Name
	FROM Role r
	INNER JOIN ParticipantRole pr ON r.Id=pr.RoleId
	INNER JOIN Participant AS p ON p.Id=pr.ParticipantId
	WHERE p.`Username`=username;
END$$
/**************************************************************************
 ** Stored Procedure: spGetProfileAnswersByParticipantId
 ** In:
 **    ParticipantId - the participant id.
 **
 ** Description:
 **    Gets a record from the ProfileAnswer table and child tables depending on the AnswerType.
 **************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetProfileAnswersByParticipantId
(IN ParticipantId	BIGINT)
BEGIN
	SELECT pa.Id, QuestionId, AnswerType, pra.Value, psa.AnswerOptionId, psia.Input
	FROM ProfileAnswer AS pa
	LEFT JOIN ProfileRangeAnswer pra ON pa.Id = pra.Id
	LEFT JOIN ProfileSingleAnswer psa ON pa.Id= psa.Id
	LEFT JOIN ProfileSingleInputAnswer psia ON psia.Id = pa.Id
	WHERE pa.ParticipantId=ParticipantId; 
END$$
/**************************************************************************
 ** Stored Procedure: spCreateProfileAnswer
 ** In:
 **    ParticipantId - the participant id.
 **    AnswerType - the answer type.
 **    QuestionId - the question id.
 **    AnswerOptionId - the answer option id.
 **    Input - the input.
 **    Value - the value.
 ** Out:
 **    Id - The Id
 ** Description:
 **    Inserts a new record into the ProfileAnswer table and child tables depending on the AnswerType.
 **************************************************************************/
DELIMITER $$
CREATE PROCEDURE spCreateProfileAnswer
(OUT Id	BIGINT,
IN ParticipantId BIGINT,
IN AnswerType VARCHAR(32),
IN QuestionId BIGINT,
IN AnswerOptionId BIGINT,
IN Input VARCHAR(128),
IN Value INT)
BEGIN
	
	INSERT INTO ProfileAnswer(QuestionId, AnswerType, ParticipantId)
	VALUES(QuestionId,AnswerType,ParticipantId);
	SET Id=LAST_INSERT_ID();
	
    IF (AnswerType='RangeAnswer') THEN
		INSERT INTO ProfileRangeAnswer(Id, Value)
		VALUES(Id,value);
	ELSEIF (AnswerType='SingleAnswerInput' OR AnswerType='SingleAnswer') THEN
			INSERT INTO ProfileSingleAnswer(Id, AnswerOptionId)
			VALUES(Id,AnswerOptionId);

		IF (AnswerType='SingleAnswerInput') THEN
			INSERT INTO ProfileSingleInputAnswer(Id, Input)
			VALUES(Id, Input);
		END IF;
	END IF;
	
END$$
/**************************************************************************
 ** Stored Procedure: spGetUserByUsername
 ** In:
 **    Username	- The username
 **
 ** Description:
 **    Gets the user by username.
 **************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetUserByUsername
(IN Username VARCHAR(64))
BEGIN
	SELECT u.Id, u.Name,u.UserPicPath FROM Users as u WHERE u.Username=Username;
END$$

/**************************************************************************
 ** Stored Procedure: spCheckParticipantUsernamePassword
 ** In:
 **    Username	- The username
 **    Password	- The Password
 **
 ** Description:
 **    Tries to see if there is a row in the ParticipantCredentials table that matches the parameter
 **************************************************************************/
DELIMITER $$
CREATE PROCEDURE spCheckParticipantUsernamePassword
(IN Username VARCHAR(64),
IN Password VARCHAR(64))
BEGIN
 IF EXISTS(SELECT * FROM ParticipantCredentials AS pc WHERE pc.Username=Username AND pc.Password=Password)  THEN 
  SELECT 1 AS IsExist;
 ELSE
	SELECT 0 AS IsExist;
 END IF;
END$$
/**************************************************************************
 ** Stored Procedure: spCheckUserUsernamePassword
 ** In:
 **    Username	- The username
 **    Password	- The Password
 **
 ** Description:
 **    Tries to see if there is a row in the User table that matches the parameter
 **************************************************************************/
DELIMITER $$
CREATE PROCEDURE spCheckUserUsernamePassword
(IN Username VARCHAR(64),
IN Password VARCHAR(256))
BEGIN
 IF EXISTS(SELECT * FROM Users AS u WHERE u.username=Username AND u.Password=Password) THEN
	SELECT 1 AS IsExist;
 ELSE
 	SELECT 0  AS IsExist;
 END If;
END$$

/**************************************************************************
 ** Stored Procedure: spSearchSurveys
 **
 ** In:
 **		Name - the survey name.
 **		PageSize - the page size.
 **		PageNumber - the 1-based page number (0 - to retrieve all records).
 **		SortColumn - the sort column.
 **		SortAscending - the sort order (1 - ascending, 0 - descending).
 ** Out:
 **    total - The total records
 **
 ** Description:
 **    Searches surveys.
 **************************************************************************/
DELIMITER $$
CREATE PROCEDURE spSearchSurveys 
	(IN Name VARCHAR(100),
	IN PageSize INT,
	IN PageNumber INT ,
	IN SortColumn VARCHAR(50),
	IN SortAscending BIT,
    OUT total BIGINT)
BEGIN
	DECLARE startCount INT;
    DECLARE endCount INT;
    DECLARE myquery VARCHAR(5000);
	    
	IF PageNumber <= 0 THEN
        SET startCount = 0;
        SET endCount = 2147483647;
    ELSE
        SET startCount = (PageNumber-1) * PageSize;
        SET endCount = PageSize;
    END IF;
    
	IF (SortColumn IS NULL OR SortColumn='') THEN
		SET SortColumn="Id";
	END IF;
    
	IF Name IS NULL THEN 
		SET Name='';
	END IF;
    
    IF SortAscending IS NULL THEN 
		SET SortAscending=1;
	END IF;
    
    CREATE TEMPORARY TABLE if not exists temp(
	  Id bigint ,
	  Name varchar(64) ,
	  CellName varchar(64) ,
	  ParticipantsNumber int,
	  Status varchar(32) ,
	  DateCreated DATETIME ,
	  CompletedSurveysNumber int,
	  Draft bit 
	);
    
   SET @myquery=CONCAT("INSERT INTO temp SELECT Id ,
			Name,
			CellName ,
			ParticipantsNumber,
			Status ,
			DateCreated ,
			CompletedSurveysNumber ,
			Draft 
            FROM Survey as s WHERE (s.Name IS NULL OR (s.Name IS NOT NULL AND s.Name LIKE '%");
            
	SET @myquery=CONCAT(@myquery,Name);
    SET @myquery=CONCAT(@myquery,"%'))");
	SET @myquery=CONCAT(@myquery," ORDER BY ");
    SET @myquery=CONCAT(@myquery,SortColumn);
    IF SortAscending=1 then
		SET @myquery=CONCAT(@myquery," ASC ");
    else
		SET @myquery=CONCAT(@myquery," DESC ");
	END IF;
      
	PREPARE stmt3 FROM @myquery;
	EXECUTE stmt3;
	DEALLOCATE PREPARE stmt3;     
    
    SELECT * FROM temp
    LIMIT startCount,  endCount;
    
    SET total= (SELECT COUNT(*) FROM temp);
    
    DROP TEMPORARY TABLE temp;
	
END$$

/**************************************************************************
 ** Stored Procedure: spGetAllSurveys
 **
 ** In:
 **		PageSize - the page size.
 **		PageNumber - the 1-based page number (0 - to retrieve all records).
 **		SortColumn - the sort column.
 **		SortAscending - the sort order (1 - ascending, 0 - descending).
 ** Out:
 **    total - The total records
 **
 ** Description:
 **    Get All Surveys.
 **************************************************************************/
DELIMITER $$
CREATE PROCEDURE spGetAllSurveys 
	(IN PageSize INT,
	IN PageNumber INT ,
	IN SortColumn VARCHAR(50),
	IN SortAscending BIT,
	OUT total BIGINT)
BEGIN
	DECLARE startCount INT;
    DECLARE endCount INT;
    DECLARE myquery VARCHAR(5000);
	    
	IF PageNumber <= 0 THEN
        SET startCount = 0;
        SET endCount = 2147483647;
    ELSE
        SET startCount = (PageNumber-1) * PageSize;
        SET endCount = PageSize;
    END IF;
    
	IF (SortColumn IS NULL OR SortColumn='') THEN
		SET SortColumn="Id";
	END IF;
	
     IF SortAscending IS NULL THEN 
		SET SortAscending=1;
	END IF;
    
    CREATE TEMPORARY TABLE if not exists temp(
	  Id bigint ,
	  `Name` varchar(64) ,
	  CellName varchar(64) ,
	  ParticipantsNumber int,
	  Status varchar(32) ,
	  DateCreated DATETIME ,
	  CompletedSurveysNumber int,
	  Draft bit 
	);
    
   SET @myquery=CONCAT("INSERT INTO temp SELECT Id ,
			s.Name,
			CellName ,
			ParticipantsNumber,
			Status ,
			DateCreated ,
			CompletedSurveysNumber ,
			Draft 
            FROM Survey s");

	SET @myquery=CONCAT(@myquery," ORDER BY ");
    SET @myquery=CONCAT(@myquery,SortColumn);
    IF SortAscending=1 then
		SET @myquery=CONCAT(@myquery," ASC ");
    else
		SET @myquery=CONCAT(@myquery," DESC ");
	END IF;
      
	PREPARE stmt3 FROM @myquery;
	EXECUTE stmt3;
	DEALLOCATE PREPARE stmt3;     
    
    SELECT * FROM temp
    LIMIT startCount,  endCount;
    
    SET total= (SELECT COUNT(*) FROM temp);
    
    DROP TEMPORARY TABLE temp;
	
END$$